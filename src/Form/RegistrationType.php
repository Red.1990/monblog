<?php

namespace App\Form;

use App\Entity\Utilisateur;
use App\Form\ApplicationType;

use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationType extends ApplicationType
{
 
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, $this->getConfiguration("Prenom", "Votre prenom..."))
            ->add('lastName', TextType::class, $this->getConfiguration("Nom", "Votre prénom ...."))
            ->add('email', EmailType::class, $this->getConfiguration("Email", "Votre adresse email.."))
            ->add('password', PasswordType::class, $this->getConfiguration("Mot de pass", "Votre mot de pass.."))
            ->add('picture', UrlType::class, $this->getConfiguration("Photo de merde", "Url de votre merdde"));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Utilisateur::class,
        ]);
    }
}
