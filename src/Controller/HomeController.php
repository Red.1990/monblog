<?php

namespace App\Controller;

use App\Entity\Ad;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{

    /**
     * @Route("/hello/{prenom}/age{age}", name="hello")
     * @Route("/hello")
     * Montre la page qui dit Bonjour
     * @return void
     */

    public function hello($prenom = "anonyme", $age = 0){
        return new Response("Bonjour " .$prenom. " Vous avez " .$age);
    }
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        $prenom = ["Lior" => 32, "Josep" => 52, "Anne" => 55];
        return $this->render('home/home.html.twig', [
            'title' => 'kjbfjk',
            'age' => 12,
            'tableau' => $prenom
        ]);
    }


    /**
     *@Route("/show", name="home_ad")
     */

    public function show(){

        $repository = $this->getDoctrine()->getRepository(Ad::class);
        $ads = $repository->findAll();

        return $this->render('home/index.html.twig',[
            'ads' =>$ads
        ]);

    }
}
